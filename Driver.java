import java.util.Scanner;
public class Driver {
    public static void main(String[] args) {
        System.out.println("Welcome to this calculator\nPlease start by adding two integers and we'll do the sum");
        Scanner reader = new Scanner(System.in);
        System.out.println(Calculator.add(reader.nextInt(), reader.nextInt()));
        System.out.println("Now input a number and we'll find it's squareroot");
        System.out.println(Calculator.sqrt(reader.nextInt()));
        System.out.println("Here's a random Number " + Calculator.random());
    }
    
}