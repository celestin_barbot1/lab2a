import java.util.Random;
public class Calculator{
    public static int add(int a, int b){
        return a + b;
    }   
    public static double sqrt(int a){
        return Math.sqrt(a);
    }   
    public static int random(){
        Random random = new Random();
        return random.nextInt();
    }   

    public static double divide(int a, int b){
        if (b == 0){
            return 1;
        }
        return a/b;
    }   
}
